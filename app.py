import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np


app = dash.Dash()
server = app.server

analysis = '''

Price Commodity Index (PCI) is a measure of the country's inflation. An increase in CPI may affect the price of goods and services this appreciating the peso value. Thus, the stronger the peso value the higher the price of our commodities..

'''

df = pd.read_csv("./assets/data.csv")

data = {
'beverage': pd.read_table('./assets/beverage.txt'),
'food': pd.read_table('./assets/food.txt'),
'fuel': pd.read_table('./assets/fuel.txt'),
'peso': pd.read_table('./assets/pesodollar.txt')
}


predictive_models = {
  'fuel': lambda x: (-2.14e-07)*(float(x)**4) + (1.15e-04)*(float(x)**3) + (-2.09e-02)*(float(x)**2) + (1.42)*(float(x)) + 19.49,
  'food': lambda x: (-1.45e-06)*(float(x)**4) + (6.21e-04)*(float(x)**3) + (-9.64e-02)*(float(x)**2) + (6.27)*(float(x)) - 93.84,
  'beverage': lambda x: (-1.74e-06)*(float(x)**4) + (5.41e-04)*(float(x)**3) + (-5.88e-02)*(float(x)**2) + (2.52)*(float(x)) +13.87
}



app.layout = html.Div([
    # represents the URL bar, doesn't render anything
    dcc.Location(id='url', refresh=False),
    # dcc.Link('Show graphy analysis', href='/graph-analysis'),
    # content will be rendered in this element
    # html.Div(id='navigation', className='navbar is-info'),
    html.Div(id='page-content', className='container'),
    # html.Div(id='predict'),
    # dcc.Dropdown(id='commodity_dropdown'),
    # dcc.Input(id='price_input')
])

app.css.append_css({'external_url': 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.css'})
app.css.append_css({'external_url': './assets/css/style.css'})
app.config['suppress_callback_exceptions']=True


@app.callback(dash.dependencies.Output('predict', 'children'),
        [ dash.dependencies.Input ('commodity_dropdown', 'value')]
    )
def get_graph_for_commodity(commodity_dropdown):
    peso = data['peso']
    commodity_data = data[commodity_dropdown]
    model = predictive_models[commodity_dropdown]

    commodity_data = commodity_data.iloc[:,0].tolist()
    predicted_data = [model(x) for x in commodity_data]

    commodity_std = np.std(predicted_data)
    min_error = [d-commodity_std for d in predicted_data]
    max_error = [d+commodity_std for d in predicted_data]

    min_error_curve = go.Scatter(
        x=commodity_data,
        y=min_error,
        name="Min error",
        line=dict(color='#00FFFF', dash='dashdot'),
        opacity=0.5
    )

    max_error_curve = go.Scatter(
        x=commodity_data,
        y=max_error,
        name="Max error",
        line=dict(color='#00FFFF', dash='dashdot'),
        opacity=0.5
    )

    trace_commodity = go.Scatter(
        x=commodity_data,
        y=peso.iloc[:, 0].tolist(),
        name=commodity_dropdown,
        mode='markers',
        marker={
            'size': 15,
            'line': {
                'width': 0.5,
                'color': 'white'
            }
        },#line=dict(color='#FFBBAF'),
        opacity=0.8
    )
    
    polynomial_curve = go.Scatter(
        # y=peso.iloc[:, 0].tolist(),
        y=predicted_data,
        x=commodity_data,
        name='Trend',
        line=dict(color='#CD0000'),
        opacity=0.8
    )

    layout = dict(
        title="Commodity Price Index VS Foreign Exchange (Peso-Dollar)",
        hovermode='closest'
    )

    fig = dict(data=[trace_commodity, polynomial_curve, min_error_curve, max_error_curve], layout=layout)

    return html.Div([
        dcc.Graph(id='my-graph', figure=fig)
    ])


@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/graph-analysis':
        df = pd.read_csv("./assets/data.csv")

        trace_peso_dollar = go.Scatter(
            x=df.date,
            y=df['pesodollar'],
            name = "Forex (Peso-Dollar)",
            line = dict(color = '#FEDDAT'),
            opacity = 0.8)

        trace_fuel = go.Scatter(
            x=df.date,
            y=df['fuel'],
            name="Fuel",
            line=dict(color='#A7EFAE'),
            opacity=0.8)

        trace_beverage = go.Scatter(
            x=df.date,
            y=df['beverage'],
            name="Beverage",
            line=dict(color='#E36692'),
            opacity=0.8)

        trace_food = go.Scatter(
            x=df.date,
            y=df['food'],
            name="Food",
            line=dict(color='#FFBBAF'),
            opacity=0.8)

        data = [trace_peso_dollar, trace_fuel, trace_beverage, trace_food]

        layout = dict(
            title='Peso Dollar over Time Period',
        )

        fig = dict(data=data, layout=layout)

        return html.Div([
            html.H3('Basic Commodity', className='container title has-text-centered', style={'marginTop': 25}),
            dcc.Graph(id='my-graph', figure=fig, className='container'),
            dcc.Markdown(children=analysis, className='container'),
            html.A('Prediction', className='button is-info', href='/', style={'marginTop': 25}),
            html.H3('Reference: Index Mundi', className='container help has-text-left', style={'marginTop': 25}),
            html.H3('2017 (C) Iskabeches', className='container help has-text-left is-info'),
        ])
    else:
        return html.Div([
            html.H3('Predict the exchange rate using the cost of a commodity.', className='title has-text-centered', style={'marginTop': 25}),
            # html.H3('Select your input', className='label has-top-padding'),
            # html.A('Price', className='button is-outlined'),
            # html.A('Exchange Rate', className='button is-outlined has-padding'),
            html.Label('Commodity', className='label has-top-padding'),
            dcc.Dropdown(
                id='commodity_dropdown',
                options=[
                    {'label': 'Food', 'value': 'food'},
                    {'label': 'Beverage', 'value': 'beverage'},
                    {'label': 'Fuel', 'value': 'fuel'}
                ],
                value='food'
            ),
            html.Div(id='predict'),
            html.Label('Price (in Peso)', className='label', style={'marginTop': 25}),
            dcc.Input(id='price_input', value='50', type='text', placeholder='Price', className='input'),
            html.Div(id='predicted_value', className='title'),
            html.A('Graph Analysis', className='button is-info', href='graph-analysis'),
            html.H3('Reference: Index Mundi', className='container help has-text-left', style={'marginTop': 25}),
            html.H3('2017 (C) Iskabeches', className='container help has-text-left is-info'),   
        ], className="container")


@app.callback(dash.dependencies.Output('predicted_value','children'),
    [dash.dependencies.Input ('commodity_dropdown', 'value'),
    dash.dependencies.Input ('price_input', 'value')])
def predict_exchange (commodity_input, price_input):
    model = predictive_models[commodity_input]
    price_output = round(model(price_input),2)
    
    return html.Div([
        html.H3('Change in foreign exchange (Peso-Dollar):', className='label', style={'marginTop': 25}),
        html.H1(price_output, className='title is-info')
    ])

if __name__ == '__main__':
    app.run_server(host="0.0.0.0", debug=True)
